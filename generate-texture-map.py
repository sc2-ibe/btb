# import struct
# from collections import namedtuple
from construct import *
import os
import png


T3_TEXTURE_HEADER = Struct("T3_TEXTURE_HEADER",
    Magic(b"MASK"),
    ULInt32("version"),
    ULInt32("unknown"),
    ULInt32("sizeX"),
    ULInt32("sizeY"),
    Padding(11 * 4)
)


class t3TextureReader(object):
    def __init__(self):
        # self.header = None
        pass

    def open(self, filename):
        self.header = T3_TEXTURE_HEADER.parse_stream(open(filename, "rb"))
        # self.data = [[None for x in range(self.header.sizeX)] for y in range(self.header.sizeY)]
        self.blocksX = int(self.header.sizeX / 8 / 8)
        self.blocksY = int(self.header.sizeX / 8 / 8)

        self.data = [
            [
                [
                    [
                        None for pixels in range(8 * 64)
                    ] for line in range(8)
                ] for block in range(self.blocksX * self.blocksY)
            ] for layer in range(8)
        ]

        f = open(filename, "rb")
        f.seek(64, os.SEEK_SET)

        for layer in range(8):
            for block in range(int(self.blocksX * self.blocksY)):
                for line in range(8):
                    for y in range(8):
                        for x in range(64):
                            absoluteX = x + int((block % (self.blocksX - 1)) * 64)
                            absoluteX += line % 64
                            absoluteY = y + int(int(block / self.blocksX) * 64)
                            absoluteY += line * 8
                            if (x % 2 == 0):
                                byte = f.read(1)
                                result = byte[0] >> 4
                            else:
                                result = byte[0] & 0x0F
                            try:
                                self.data[layer][block][line][y * 64 + x] = result
                                # self.data[absoluteY][absoluteX] = byte
                            except IndexError:
                                print(block)
                                print(line)
                                print(absoluteY)
                                print(absoluteX)
                                exit(1)

        f.close()

    def getLayerBlendingAt(self, layer, x, y):
        block = int(int(y / 64) * self.blocksX)
        block += int(x / 64)
        x %= 64
        y %= 64
        line = int(y / 8)
        y %= 8
        return self.data[layer][block][line][y * 64 + x]

    def getBoldestLayerAt(self, x, y):
        bestValue = None
        bestId = None
        for layer in range(8):
            value = self.getLayerBlendingAt(layer, x, y)
            # print(value)
            if layer == 0 or value > bestValue:
                bestValue = value
                bestId = layer
        return bestId

    def getLayerAtPoint(self, x, y):
        return self.getBoldestLayerAt(int(y * 8), int(x * 8))


def writeScript(filename, t3Reader):
    f = open(filename, "w")
    f.write(
        "\n" +
        ("string[%d] tMap;\n" % t3Reader.header.sizeY) +
        "void initTextureMap() {\n"
    )
    for y in range(t3Reader.header.sizeY):
        buffer = ""
        f.write("    tMap[%d] = " % y)
        for x in range(t3Reader.header.sizeX):
            if x >= 1024:
                break
            buffer += str(t3Reader.getBoldestLayerAt(x, y))
        f.write('"%s"' % buffer)

        if (t3Reader.header.sizeX > 1024):
            buffer = ""
            for x in range(t3Reader.header.sizeX - 1024):
                buffer += str(t3Reader.getBoldestLayerAt(x + 1024, y))
            f.write(' + \n    "%s"' % buffer)

        f.write(";\n")

    f.write("}\n")
    f.close()


def writeImage(filename, t3Reader):
    s = []
    for y in range(0, t3Reader.header.sizeX):
        row = []
        for x in range(0, t3Reader.header.sizeX):
            row.append(t3Reader.getBoldestLayerAt(x, y) * 8)
        s.append(row)

    f = open(filename, 'wb')
    w = png.Writer(len(s[0]), len(s), greyscale=True, bitdepth=8)
    w.write(f, s)
    f.close()

if __name__ == '__main__':
    print("Reading file..")
    t3Reader = t3TextureReader()
    t3Reader.open("map.SC2Map/t3TextureMasks")
    # t3Reader.open("text test.SC2Map/t3TextureMasks")

    # t3Reader.getBoldestLayerAt(1, 1)
    # t3Reader.getLayerAtPoint(18.3, 10.6)
    # exit(0)

    print("Generating script..")
    writeScript("map.SC2Map/TextureMap.galaxy", t3Reader)

    # print("Generating image..")
    # writeImage("TextureMap.png", t3Reader)
