include "scripts/header"

static int ui_sbButton;

static int ui_sbGetFreeSlot() {
    int i;

    for (i = 0; i < MAX_PLAYERS; i += 1) {
        if (ui_scoreBoard.rows[i].player > 0) { continue; }
        return i;
    }
    
    return -1;
}

static int ui_sbGetPlayerSlot(int playerId) {
    int i;

    for (i = 0; i < MAX_PLAYERS; i += 1) {
        if (ui_scoreBoard.rows[i].player != playerId) { continue; }
        return i;
    }
    
    return -1;
}


void ui_scoreBoardInit() {
    int i;

    ui_scoreBoard.dialog = DialogCreate(
        0, 0,
        c_anchorCenter,
        0, 0,
        true
    );
    DialogSetImageVisible(ui_scoreBoard.dialog, false);
    DialogSetFullscreen(ui_scoreBoard.dialog, true);
    ui_scoreBoard.panel = DialogControlCreateFromTemplate(ui_scoreBoard.dialog, c_triggerControlTypePanel, "ScoreBoard/ScoreBoardTemplate");

    for (i = 0; i < MAX_PLAYERS; i += 1) {
        DialogControlHookup(ui_scoreBoard.panel, c_triggerControlTypeLabel, "TableRow0" + IntToString(i) + "/Player");
        ui_scoreBoard.rows[i].playerNameLabel = DialogControlLastCreated();
        DialogControlHookup(ui_scoreBoard.panel, c_triggerControlTypeLabel, "TableRow0" + IntToString(i) + "/Revives");
        ui_scoreBoard.rows[i].revivesLabel = DialogControlLastCreated();
        DialogControlHookup(ui_scoreBoard.panel, c_triggerControlTypeLabel, "TableRow0" + IntToString(i) + "/Deaths");
        ui_scoreBoard.rows[i].deathsLabel = DialogControlLastCreated();
        DialogControlHookup(ui_scoreBoard.panel, c_triggerControlTypeLabel, "TableRow0" + IntToString(i) + "/RDRatio");
        ui_scoreBoard.rows[i].rdRatioLabel = DialogControlLastCreated();
        DialogControlHookup(ui_scoreBoard.panel, c_triggerControlTypeLabel, "TableRow0" + IntToString(i) + "/LevelsCompleted");
        ui_scoreBoard.rows[i].levelsCompletedLabel = DialogControlLastCreated();
        DialogControlHookup(ui_scoreBoard.panel, c_triggerControlTypeLabel, "TableRow0" + IntToString(i) + "/BonusesTaken");
        ui_scoreBoard.rows[i].bonusesTakenLabel = DialogControlLastCreated();
        DialogControlHookup(ui_scoreBoard.panel, c_triggerControlTypeLabel, "TableRow0" + IntToString(i) + "/CurrentLevel");
        ui_scoreBoard.rows[i].currentLevelLabel = DialogControlLastCreated();
    }

    DialogControlCreateInPanelFromTemplate(
        ui_consoleContainer,
        c_triggerControlTypeButton,
        "GamePanels/ScoreBoardButton"
    );
    DialogControlSetPosition(
        DialogControlLastCreated(),
        PlayerGroupAll(),
        c_anchorBottomRight,
        25, 80
    );
    ui_sbButton = DialogControlLastCreated();

    TriggerAddEventDialogControl(TriggerCreate("ui_onScoreboardButton"), c_playerAny, ui_sbButton, c_triggerControlEventTypeClick);
}

void ui_scoreBoardRefreshPlayer(int playerId) {
    int rowId;
    fixed ratio;

    rowId = ui_sbGetPlayerSlot(playerId);

    if (playerId == 0) {
        // ui_setLabelText(ui_scoreBoard.rows[rowId].playerNameLabel, StringToText("-"));
    }
    else {
        if (st_stats[playerId].session.deaths > 0) {
            ratio = IntToFixed(st_stats[playerId].session.revives) / IntToFixed(st_stats[playerId].session.deaths);
        }
        else {
            ratio = IntToFixed(st_stats[playerId].session.revives);
        }

        ui_setLabelText(ui_scoreBoard.rows[rowId].playerNameLabel, PlayerName(playerId));
        ui_setLabelText(ui_scoreBoard.rows[rowId].revivesLabel, IntToText(st_stats[playerId].session.revives));
        ui_setLabelText(ui_scoreBoard.rows[rowId].deathsLabel, IntToText(st_stats[playerId].session.deaths));
        ui_setLabelText(ui_scoreBoard.rows[rowId].rdRatioLabel, FixedToText(
            ratio, 2
        ));
        ui_setLabelText(ui_scoreBoard.rows[rowId].levelsCompletedLabel, IntToText(st_stats[playerId].session.levelsCompleted));
        ui_setLabelText(ui_scoreBoard.rows[rowId].bonusesTakenLabel, IntToText(st_stats[playerId].session.bonusesTaken));
        ui_setLabelText(ui_scoreBoard.rows[rowId].currentLevelLabel, FixedToText(
            UnitGetPropertyFixed(gm_players[playerId].hero.mainUnit, c_unitPropLevel, true), -1
        ));

        ui_setLabelColor(ui_scoreBoard.rows[rowId].playerNameLabel, libNtve_gf_ConvertPlayerColorToColor(PlayerGetColorIndex(playerId, false)));
        ui_setLabelColor(ui_scoreBoard.rows[rowId].revivesLabel, libNtve_gf_ConvertPlayerColorToColor(PlayerGetColorIndex(playerId, false)));
        ui_setLabelColor(ui_scoreBoard.rows[rowId].deathsLabel, libNtve_gf_ConvertPlayerColorToColor(PlayerGetColorIndex(playerId, false)));
        ui_setLabelColor(ui_scoreBoard.rows[rowId].rdRatioLabel, libNtve_gf_ConvertPlayerColorToColor(PlayerGetColorIndex(playerId, false)));
        ui_setLabelColor(ui_scoreBoard.rows[rowId].levelsCompletedLabel, libNtve_gf_ConvertPlayerColorToColor(PlayerGetColorIndex(playerId, false)));
        ui_setLabelColor(ui_scoreBoard.rows[rowId].bonusesTakenLabel, libNtve_gf_ConvertPlayerColorToColor(PlayerGetColorIndex(playerId, false)));
        ui_setLabelColor(ui_scoreBoard.rows[rowId].currentLevelLabel, libNtve_gf_ConvertPlayerColorToColor(PlayerGetColorIndex(playerId, false)));
    }
}

void ui_scoreBoardRedraw() {
    int i;
    int playerId;
    int rowId;

    for (i = 0; i < MAX_PLAYERS; i += 1) {
        ui_scoreBoard.rows[i].player = 0;
    }

    for (i = 1; i <= MAX_PLAYERS; i += 1) {
        if (!gm_players[i].active) { continue; }
        rowId = ui_sbGetFreeSlot();
        ui_scoreBoard.rows[rowId].player = i;
    }

    for (i = 0; i < MAX_PLAYERS; i += 1) {
        playerId = ui_scoreBoard.rows[i].player;
        ui_scoreBoardRefreshPlayer(playerId);
    }
}

void ui_scoreBoardShow(int player, bool show) {
    DialogSetVisible(ui_scoreBoard.dialog, PlayerGroupSingle(player), show);
}

bool ui_onTabDown(bool testConds, bool runActions) {
    ui_scoreBoardShow(EventPlayer(), true);
    return true;
}

bool ui_onTabUp(bool testConds, bool runActions) {
    ui_scoreBoardShow(EventPlayer(), false);
    return true;
}

bool ui_onScoreboardButton(bool testConds, bool runActions) {
    if (EventDialogControl() != ui_sbButton) { return false; }

    if (!DialogIsVisible(ui_scoreBoard.dialog, EventPlayer())) {
        DialogSetVisible(ui_scoreBoard.dialog, PlayerGroupSingle(EventPlayer()), true);
    }
    else {
        DialogSetVisible(ui_scoreBoard.dialog, PlayerGroupSingle(EventPlayer()), false);
    }
    return true;
}
