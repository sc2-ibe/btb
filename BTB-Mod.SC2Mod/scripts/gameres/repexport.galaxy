static const string ASCII_TABLE = "\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x40\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x5b\x5c\x5d\x5e\x5f\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x78\x79\x7a\x7b\x7c\x7d\x7e\x7f\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8a\x8b\x8c\x8d\x8e\x8f\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f\xa0\xa1\xa2\xa3\xa4\xa5\xa6\xa7\xa8\xa9\xaa\xab\xac\xad\xae\xaf\xb0\xb1\xb2\xb3\xb4\xb5\xb6\xb7\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff";

static const string UT_BEGIN = "__";
static const string UT_DATA = "_";
static const int RP_BUFF_SIZE = 8192;

static unit rp_marker_unit;
static byte[RP_BUFF_SIZE] rp_buff;
static int rp_blen;

static int ascii_from(string s, int index)
{
    return StringFind(ASCII_TABLE, StringSub(s, index, index), c_stringCase);
}

static void ucreate(string utype, byte a, byte b, byte c, byte d)
{
    UnitCreate(1, utype, c_unitCreateIgnorePlacement, 15, Point(a, b), 270.0);
    UnitSetPosition(UnitLastCreated(), Point(c, d), false);
    UnitRemove(UnitLastCreated());
}

void repx_flush()
{
    int i = 0;
    int l;
    int[4] tmp_p;

    UnitCreate(1, UT_BEGIN, c_unitCreateIgnorePlacement, 15, Point((rp_blen >> 8) & 0xFF, rp_blen & 0xFF), 270.0);
    rp_marker_unit = UnitLastCreated();

    while (i < rp_blen) {
        for (l = 0; l < 4; l += 1) {
            if (i < rp_blen) {
                tmp_p[l] = rp_buff[i];
            }
            else {
                tmp_p[l] = 0;
            }
            i += 1;
        }
        ucreate(UT_DATA, tmp_p[0], tmp_p[1], tmp_p[2], tmp_p[3]);
    }

    UnitRemove(rp_marker_unit);
    rp_marker_unit = null;

    rp_blen = 0;
}

void repx_push_byte(byte data)
{
    rp_buff[rp_blen] = data;
    rp_blen += 1;
}

void repx_push_bool(bool data)
{
    if (data) {
        repx_push_byte(1);
    }
    else {
        repx_push_byte(0);
    }
}

void repx_push_int8(int data)
{
    repx_push_byte(data & 0xFF);
}

void repx_push_int16(int data)
{
    repx_push_byte((data >>  8) & 0xFF);
    repx_push_byte(data & 0xFF);
}

void repx_push_int32(int data)
{
    repx_push_byte((data >> 24) & 0xFF);
    repx_push_byte((data >> 16) & 0xFF);
    repx_push_byte((data >>  8) & 0xFF);
    repx_push_byte(data & 0xFF);
}

void repx_push_fixed32(fixed data)
{
    // ---------------------------------------------------
    // -- 1 sign | 19 integer part | 12 fractional part --
    // ---------------------------------------------------
    int tmp = 0;

    if (data < 0.0) {
        tmp |= 1 << 31;
    }
    tmp |= AbsI(TruncI(data)) << 12;
    tmp |= FixedToInt(AbsF(data - Trunc(data)) * 4096.0) & 0xFFF;

    repx_push_int32(tmp);
}
