NOW:

NEXT:
 * camera pan check camera distance
 * lvl12 secret path spawn more probes
 * neutral unit selected ui
 * Improve commands
 * Scoreboard:
    - Gray out & clear players that left the game
 * Game over - sound & text write out?
 * boost behavior not visible
 * -afk
 * stalkers minimap
 * q & art lvl2

POSTPONED:
 * Possiblity to disable camera pan
 * Reaver
 * Level up actor; different one for bonus?
 * ART ability
    - limit to x (3?) revives, but longer timeout?
 * Revive coin:
    - Egg higher radius, other visual impact
 * Options dialog
 * Stats system
 * Stats dialog
 * Customizable game info messages - revives, level completing etc.
 * Catapults:
    - Entry angle impacts on landing point
    - Don't jump on chained catapult if still in air
    - Shade compatibility
    - Different height jumping
    - Follow cam on chained catapults
    - Allow obstacles to jump; store inAir flag in unit custom values
    - Choose animation basing on height/distance ratio
    - Fix model's origin point
    - Calculate region bounds automatically
 * Portal:
    - Allow target as object
 * Buttons:
    - Other sounds for button?
    - Smooth animation transition for protoss button
 * Objects - dedicated array for every type
 * Revise if every ability has a sound
 * Direction arrow:
    - auto rotate
 * Stats:
    - Level finish streak
    - Achievements?

ENVIRONMENT FEATURES:
 * Gravity disruptors
 * Ibe2 flame like thing - lasers / energy discharges
 * Missile launcher object
 * Artillery area object
 * Spikes
 * Punch hand like object
 * Moving ice:
    - Need to make new splat model
 
MAYBE:
 * Stone ability
 * Visual indicator for radius based collision shapes
 * End game cinematic - carrier flying over the maps with FPS perpsective
 * Manual calculation of bounce vector
 * Rectangle based collision shape for some obstacles
 * Banks & encryption system
 * Ice boost ability:
    - Blur unit actor?